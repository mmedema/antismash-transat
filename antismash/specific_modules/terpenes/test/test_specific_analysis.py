import unittest
from antismash.specific_modules.terpenes.specific_analysis import (
    store_terp_predicat_results,
    TerpenePredicatPrediction,
)

class FakeTerpene(object):
    """Fake SeqFeature class for testing"""
    def __init__(self, locus_tag):
        self.qualifiers = {'locus_tag': [locus_tag]}


class TestTerpenes(unittest.TestCase):
    def test_store_terp_predicat_results(self):
        inputs = {
            'mic': 'mic\t1-6-MTC\tno_force_needed\t0.275104906001\t0.8899580376\t0.8899580376\t2-Methylisoborneol Synthase\tPseudoanabaena limnetica Castaic Lake\tlinear\tADU79148\t97.2\tADU79148_1-6-MTC\tmic_UNK\n',
            'locus_0123': 'ctg1_5124\t1-11-STC\tno_force_needed\t0.0\t1.0\t19.376586372\tNA\tNA\tNA\tNA\t100.0\trefa-WP-023609483_1-11-STC\tlocus_0123_UNK\n',
            'locus_1234': 'ctg1_705\t1-10-STC\tno_force_needed\t1.09281061317e-07\t0.999999956288\t23.8565629375\t(+)-epi-Cubenol Synthase\tStreptomyces sp. HCCB10043\tlinear\tWP_023609332\t100.0\tWP-023609332_1-10-STC\tlocus_1234_UNK\n'
        }

        mic = FakeTerpene("mic")
        locus_0123 = FakeTerpene("locus_0123")
        locus_1234 = FakeTerpene("locus_1234")
        terpenes = [mic, locus_0123, locus_1234]

        store_terp_predicat_results(terpenes, inputs)
        assert 'note' in mic.qualifiers
        assert 'note' in locus_0123.qualifiers
        assert 'note' in locus_1234.qualifiers


class TestTerpenePredicatPrediction(unittest.TestCase):
    def test_init(self):
        mic = 'mic\t1-6-MTC\tno_force_needed\t0.275104906001\t0.8899580376\t0.8899580376\t2-Methylisoborneol Synthase\tPseudoanabaena limnetica Castaic Lake\tlinear\tADU79148\t97.2\tADU79148_1-6-MTC\tmic_UNK\n'
        geo = 'Npun_R2756\t1-10-STC\tno_force_needed\t0.0\t1.0\t1.0\tGeosmin Synthase\tNostoc punctiforme PCC73102\tlinear\tYP_001866236\t100.0\tYP-001866236_1-10-STC\tNpun_R2756_UNK\n'

        pred = TerpenePredicatPrediction(mic)
        self.assertEqual(pred.cyclization_pattern, "1-6-MTC")
        self.assertEqual(pred.nearest_neighbour, "ADU79148, Pseudoanabaena limnetica Castaic Lake")
        self.assertEqual(pred.percent_identity, "97.2")

        pred = TerpenePredicatPrediction(geo)
        self.assertEqual(pred.cyclization_pattern, "1-10-STC")
        self.assertEqual(pred.nearest_neighbour, "YP_001866236, Nostoc punctiforme PCC73102")
        self.assertEqual(pred.percent_identity, "100.0")
