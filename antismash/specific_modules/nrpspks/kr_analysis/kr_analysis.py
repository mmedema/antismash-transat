## Author: Marnix Medema
## University of Groningen
## Department of Microbial Physiology / Groningen Bioinformatics Centre

from antismash import utils

def run_kr_analysis(infile2, out_file):
    ##Core script
    #Extract activity and stereochemistry signatures from KR domains
    infile = utils.get_full_path(__file__, "KRdomains_muscle.fasta")
    muscle_file = "muscle.fasta"
    dict2 = utils.read_fasta(infile2)
    namesb = dict2.keys()
    seqsb = dict2.values()
    querysignames = []
    querysigseqs_act = []
    querysigseqs_ste = []
    for i in namesb:
      seq = seqsb[namesb.index(i)]
      querysignames.append(i)
      utils.writefasta([i],[seq],"infile.fasta")
      infile2 = "infile.fasta"
      refsequence = "MAPSI|PKS|CAM00062.1|Erythromycin_synthase_modules_1_and_2|Sacc_KR1"
      namesa = [i]
      #Run muscle and collect sequence positions from file
      utils.execute(["muscle", "-profile", "-quiet", "-in1", infile, "-in2", infile2, "-out", "muscle.fasta"])
      positions_act = [110,134,147,151]
      positions_ste = [90,91,92,139,144,147,149,151]
      #Count residues in ref sequence and put positions in list
      muscle_dict = utils.read_fasta(muscle_file)
      #Extract activity signature
      refseq = muscle_dict[refsequence]
      poslist_act = []
      b = 0
      c = 0
      while refseq != "":
        i = refseq[0]
        if c in positions_act and i != "-":
          poslist_act.append(b)
        if i != "-":
          c += 1
        b += 1
        refseq = refseq[1:]
      #Extract stereochemistry signature
      refseq = muscle_dict[refsequence]
      poslist_ste = []
      b = 0
      c = 0
      while refseq != "":
        i = refseq[0]
        if c in positions_ste and i != "-":
          poslist_ste.append(b)
        if i != "-":
          c += 1
        b += 1
        refseq = refseq[1:]
      #Extract positions from query sequence
      query = namesa[0]
      query_seq = muscle_dict[query]
      seq_act = ""
      seq_ste = ""
      for j in poslist_act:
        aa = query_seq[j]
        seq_act = seq_act + aa
      querysigseqs_act.append(seq_act)
      for j in poslist_ste:
        aa = query_seq[j]
        seq_ste = seq_ste + aa
      querysigseqs_ste.append(seq_ste)

    #Check activity
    activitydict = {}
    for i in querysignames:
      querysigseq_act = querysigseqs_act[querysignames.index(i)]
      activity = "inactive"
      if querysigseq_act[0] == "K" and (querysigseq_act[1] == "S" or querysigseq_act[1] == "A" or querysigseq_act[1] == "G") and querysigseq_act[2] == "Y" and querysigseq_act[3] == "N":
        activity = "active"
      if querysigseq_act[0] == "E" and (querysigseq_act[1] == "S" or querysigseq_act[1] == "A" or querysigseq_act[1] == "G") and querysigseq_act[2] == "H" and querysigseq_act[3] == "H":
        activity = "active"
      if querysigseq_act[0] == "K" and (querysigseq_act[1] == "S" or querysigseq_act[1] == "A" or querysigseq_act[1] == "G") and querysigseq_act[2] == "Y" and (querysigseq_act[3] == "N" or querysigseq_act[3] == "G"):
        activity = "active"
      activitydict[i] = activity

    #Predict stereochemistry
    stereodict = {}
    for i in querysignames:
      querysigseq_ste = querysigseqs_ste[querysignames.index(i)]
      if querysigseq_ste[0:3] != "LDD" and querysigseq_ste[3] == "W" and querysigseq_ste[4] != "H" and querysigseq_ste[5:] == "YAN":
        stereochemistry = "A1"
      elif querysigseq_ste[0:3] != "LDD" and querysigseq_ste[3:] == "WHYAN":
        stereochemistry = "A2"
      elif querysigseq_ste[0:3] == "LDD" and querysigseq_ste[5] == "Y" and querysigseq_ste[6] != "P" and querysigseq_ste[7] == "N":
        stereochemistry = "B1"
      elif querysigseq_ste[0:3] == "LDD" and querysigseq_ste[5:] == "YPN":
        stereochemistry = "B2"
      elif querysigseq_ste[5] != "Y":
        stereochemistry = "C1"
      elif querysigseq_ste[5] == "Y" and querysigseq_ste[7] != "N":
        stereochemistry = "C2"
      else:
        stereochemistry = "?"
      stereodict[i] = stereochemistry

    #Output to file
    outfile = open(out_file,"w")
    for i in querysignames:
      outfile.write(i + "\t" + activitydict[i] + "\t" + stereodict[i] + "\n")
