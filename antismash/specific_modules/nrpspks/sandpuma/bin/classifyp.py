from sklearn import tree
import csv
import sys
import numpy as np

## Define params
msl = int(sys.argv.pop())	## sklearn default=1
md = int(sys.argv.pop())	## sklearn default='None'

## Read in features
features = []
with open('etmp.features.tsv','rb') as tsvin:
	tsvin = csv.reader(tsvin, delimiter='\t')
	for row in tsvin:
		features.append(row)
## Read in labels
labels = []
with open('etmp.labels.tsv','rb') as tsvin:
	tsvin = csv.reader(tsvin, delimiter='\t')
	for row in tsvin:
		labels.append(row[0])
## Train the decision tree
clf = tree.DecisionTreeClassifier(min_samples_leaf=msl, max_depth=md)
clf = clf.fit(features, labels)

## Make prediction
print clf.predict([sys.argv[1:]])[0]
