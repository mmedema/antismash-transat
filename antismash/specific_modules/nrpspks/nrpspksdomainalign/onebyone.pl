#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $faf = './data/KS_rawseq_pred_training_transATPKS.txt';
my $faa = new Bio::SeqIO(-file=>$faf, -format=>'fasta');
chomp(my $tot = `grep -c '>' $faf`);
my $n = 1;
my $out = './data/KS_fullclades.tsv';
system("rm $out") if(-e $out);
while(my $seq = $faa->next_seq){
    print "$n of $tot complete\n" if($n % 500 == 0);
    open my $tfh, '>', 'tmp.faa' or die $!;
    print $tfh '>'.$seq->id."\n".$seq->seq."\n";
    close $tfh;
    system("python substrate_from_faa.py tmp.faa >> $out");
    $n += 1;
}
