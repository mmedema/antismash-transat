#!/bin/env perl

use strict;
use warnings;

my @j = (0.00);
my @d = (0.32);
my @g = (0.00);
my @a = (0.68);

for(my $i=0;$i<scalar(@j);$i += 1){
    my $dir = 'j'.100*$j[$i].'g'.100*$g[$i].'d'.100*$d[$i].'a'.100*$a[$i];
    print $dir."\n";
    if(-d $dir){
	next;
    }else{
	mkdir $dir;
	system("python generate_dendrogram_userweights.py $j[$i] $g[$i] $d[$i] $a[$i]");
	system("perl tree_rn_species.pl");
	system("perl generate_itol.pl > itol.txt");
	system("mv *txt $dir");
	system("mv *nwk $dir");
    }
}
