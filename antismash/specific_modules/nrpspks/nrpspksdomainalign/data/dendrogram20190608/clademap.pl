#!/bin/env perl

use strict;
use warnings;

my %seen = ();
open my $afh, '<', 'Annotation_MC_final.txt' or die $!;
while(<$afh>){
    chomp;
    next if($_ =~ m/^#/);
    my ($leaf, $clade, $desc) = split(/\t/, $_);
    next if($desc =~ m/^\s*$/);
    my $cnum = $clade;
    $cnum =~ s/.+_(\d+)$/$1/;
    unless(exists $seen{$cnum}){
	$seen{$cnum} = $desc;
    }
}
close $afh;

foreach my $c (sort {$a <=> $b} keys %seen){
    print join("\t", 'Clade_'.$c, $seen{$c})."\n";
}
