#!/bin/env python

import os

def stk_to_algn(stkFiles):
    for stkFile in stkFiles:
        with open(stkFile, 'r') as infile:
            algnDict = {}
            for line in infile:
                if line.startswith("#"):
                    continue
                elif re.match("^[\s\/]", line[0]) is None:
                    header = line.split(' ')[0]
                    algn = line.split(' ')[-1]
                    algn = ''.join([pos for pos in algn if (pos.isupper() or pos == '-')])
                    if header in algnDict.keys():
                        algnDict[header] += algn
                    else:
                        algnDict[header] = ">%s\n%s" %(header, algn)
        #outfile.write("\n".join(algnDict.values()))
        print "\n".join(algnDict.values())
    return

stk_to_algn(['tmp.stk'])
