PYTHON_FILES=$(shell find . -name '*.py')

all: unit coverage

tags: ${PYTHON_FILES}
	ctags --python-kinds=-i ${PYTHON_FILES}

coverage: ${PYTHON_FILES}
	rm -rf cover
	mkdir cover
	nosetests -v --with-coverage --cover-html --cover-package="antismash"

unit: ${PYTHON_FILES}
	nosetests -v

integration: ${PYTHON_FILES}
	nosetests -v -m "(?:^|[\b_\./-])[Ii]ntegration"

clean:
	./scripts/cleanup.sh

docker:
	./scripts/cleanup.sh
	docker build -t antismash-dev .

docker-nocache:
	./scripts/cleanup.sh
	docker build -t antismash-dev --no-cache .

as_dev_image.tar.xz:
	./scripts/cleanup.sh
	docker build -t antismash-dev .
	docker tag antismash-dev:latest antismash/dev-image:latest
	docker save -o as_dev_image.tar antismash/dev-image:latest
	pxz as_dev_image.tar

docker-export: as_dev_image.tar.xz

.PHONY: coverage unit integration clean docker docker-export
